import React from "react";

import "./WeAre.css";

function WeAre() {
  return (
    <div>
      <div className="row mt-4 pb-4">
        <div className="col-lg-3">
          <div class="card text-center">
            <div class="card-body">
              <h5 class="card-title">Importer</h5>
              <p class="card-text">
                Know more about how to import world-class tiles to your country.
              </p>
              <p class="card-text">
                <small class="muted">Know more</small>
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-3">
          <div class="card text-center">
            <div class="card-body">
              <h5 class="card-title">Distributor</h5>
              <p class="card-text">
                Be a part of our ever growing distribution network across India.
              </p>
              <p class="card-text">
                <small class="muted">know more</small>
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-3">
          <div class="card text-center">
            <div class="card-body">
              <h5 class="card-title">Builder</h5>
              <p class="card-text">
                Planning your next big building project? Our tiles are made for
                you.
              </p>
              <p class="card-text">
                <small class="muted">know more</small>
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-3">
          <div class="card text-center">
            <div class="card-body">
              <h5 class="card-title">Home/Office Owner</h5>
              <p class="card-text">
                Get your house walls and floor embellished by our tiles.
              </p>
              <p class="card-text">
                <small class="muted">know more</small>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default WeAre;
